import sys
import wave
import pulseaudio as pa
import numpy as np
from bitarray import bitarray
import binascii
(nchannels, sampwidth, sampformat, framerate, length) = (1, 2, pa.SAMPLE_S16LE, 44100, 1)
#4b5b dictionary
dic = {'0000' : '11110', '0001' : '01001',
        '0010' : '10100', '0011' : '10101',
        '0100' : '01010', '0101' : '01011',
        '0110' : '01110', '0111' : '01111',
        '1000' : '10010', '1001' : '10011',
        '1010' : '10110', '1011' : '10111',
        '1100' : '11010', '1101' : '11011',
        '1110' : '11100', '1111' : '11101'}
        
#4b5b^-1 dictionary
dic2 = { dic[x] : x for x in dic }

#Function counting and returning crc
def crc_fun(bitarr):
    crc = binascii.crc32(bitarr.tobytes()) % (1<<32)
    crc = bitarray(bin(crc)[2:], endian = 'big')
    return (32 - len(crc)) * bitarray('0') + crc

#Function returning freq and highest fft value
def helper(time):
	
    nframes = int(time * recorder.rate)
    data = recorder.read(nframes)
    
    #Reading highest fft value, and frequency and returning it
    l = np.fft.fft(data)
    i = 0
    val = l[0].real*l[0].real + l[0].imag * l[0].imag
    for j in range(1,len(l)/2):
        if l[i].real*l[i].real + l[i].imag*l[i].imag < l[j].real*l[j].real + l[j].imag * l[j].imag:
            val = l[j].real*l[j].real + l[j].imag * l[j].imag
            i = j
            
    return (float(i/time), val)
 
#Function decoding Ethernet Frame to informations and message
def decode(code):

	#cutting one bit if garbage info appears
    if len(code) % 5 == 1:
        code = code[0 : len(code) - 1]
        
    #decoding nrz, just simple nrz^-1
    last = "1"
    i = 0
    length = len(code)
    while i < length:
        if code[i : i + 1] == "1":
            bit = "0"
            if last == "0":
                bit = "1"
            code = code[0 : i] + bit + code[i + 1 :]
            last = "1"
        else:
            code = code[0 : i] + last + code[i + 1 :]
            last = "0"
        i += 1
       
    #decoding 4b5b, simple 4b5b^-1
    i = 0
    length = len(code) * 4/5
    while i < length:
        code = code[ : i] + dic2[code[i : i + 5]] + code[i + 5 :]
        i += 4
        
    #decoding adresses and length from bits to ints
    num1 = code[0 : 48]
    num2 = code[48 : 96]
    length = code[96 : 112]
    num1 = int(num1, 2)
    num2 = int(num2, 2)
    length = int(length, 2)
    
    #checking if message was propper
    if len(code) != 144 + length * 8:
        return
        
    #Checking if crc is fine and printing result
    message = bitarray(code[112 : (112 + length * 8)])
    message = message.tobytes()
    crc = crc_fun(bitarray(code[0 : (112 + length * 8)]))
    crc = crc.to01()
    if crc == code[(112 + length * 8):]:
        print(str(num2) +  " " + str(num1) +" "+ message.decode('utf-8'))
       
#reading variables from arguments
time = float(sys.argv[1])
time = float(1/time)
freq1 = int(sys.argv[2])
freq2 = int(sys.argv[3])

with pa.simple.open(direction=pa.STREAM_RECORD, format=sampformat, rate=framerate, channels=nchannels) as recorder:
    #Passing through noises
    while(True):
        (freq, val) = helper(time)
        if freq > 5:
		    break
		    
    #Trying to find best matching
    results = []
    for i in range(0,5):
	    results.append(helper(time)[1])
	    helper(time * 0.2)
    j = 0
    for i in range(1,5):
        if results[i] > results[j]:
            j = i
    for i in range(0,j):
		helper(time * 0.2)

    #going through preambula and checking for 11
    last = 0
    while(True):
        (freq, val) = helper(time)
        if abs(freq - freq2) < 4 and abs(last - freq2) < 4:
            break
        last = freq
    
    #reading message and passing it to decode function
    mess = []
    while(True):
        try:
            (freq, val) = helper(time)
            if abs(freq - freq1) < 4:
				mess.append("0")
            else: 
				if abs(freq - freq2) < 4:
				    mess.append("1")
        except Exception:
            mess = ''.join(mess)
            decode(mess)
            exit()
		
		
    
    
