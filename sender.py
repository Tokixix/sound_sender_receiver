from bitarray import bitarray
import binascii
import sys
import time
import wave
import pulseaudio as pa
import numpy as np
#4b5b dictionary
dic = {'0000' : '11110', '0001' : '01001',
        '0010' : '10100', '0011' : '10101',
        '0100' : '01010', '0101' : '01011',
        '0110' : '01110', '0111' : '01111',
        '1000' : '10010', '1001' : '10011',
        '1010' : '10110', '1011' : '10111',
        '1100' : '11010', '1101' : '11011',
        '1110' : '11100', '1111' : '11101'}
        
#4b5b^-1 dictionary
dic2 = { dic[x] : x for x in dic }

#preambula
preambula = '10' * 31  + '11'

#Function counting and returning crc
def crc_fun(bitarr):
    crc = binascii.crc32(bitarr.tobytes()) % (1<<32)
    crc = bitarray(bin(crc)[2:], endian = 'big')
    return (32 - len(crc)) * bitarray('0') + crc
    
#Function counting Ethernet Frame
def encode(first, second, message):
    result = bitarray(endian = 'big')
    
    #Coding adresses into bits and adding 0
    first = bitarray(bin(first)[2:], endian = 'big')
    second = bitarray(bin(second)[2:], endian = 'big')
    first = (48 - len(first)) * bitarray('0') + first
    second = (48 - len(second)) * bitarray('0') + second
    
    #Coding length of message into bits and adding 0
    length = bitarray(bin(len(message))[2:], endian  = 'big')
    length = (16 - len(length)) * bitarray('0') + length
    

    #Coding message to bits, adding all and crc
    b = bitarray()
    b.fromstring(message)
    result = second + first + length + b
    result += crc_fun(result)
    
    #4b5b on result
    i = 0
    result = result.to01()
    length = len(result)* 5/4
    while i < length:
        result = result[0 : i] + dic[result[i : i + 4]] + result[i + 4 :]
        i += 5
    
    #nrz on result
    last = "1"
    i = 0
    while i < length:
        if result[i : i+1] == last:
            last = "0"
            result = result[0 : i] + "0" + result[i + 1 :]
        else:
            last = "1"
            result = result[0 : i] + "1" + result[i + 1 :]
        i += 1
        
    return (preambula + result)

# Reading freqs and time from arguments
sample_map = {
	1 : pa.SAMPLE_U8,
    2 : pa.SAMPLE_S16LE,
    4 : pa.SAMPLE_S32LE,
}
time = float(sys.argv[1])
time = 1/time
freq1 = int(sys.argv[2])
freq2 = int(sys.argv[3])

while 1:
    try:
		#reading info and creating Eth Frame
        inp = raw_input()
        inp = inp.split(' ')
        code = encode(int(inp[0]), int(inp[1]), " ".join(inp[2 :]))
        #opening player and adding frames of sound
        with pa.simple.open(direction=pa.STREAM_PLAYBACK, format=sample_map[2], rate=44100, channels=1) as player:
            i = 0
            while i < len(code):
                freq = freq2
                if code[i : i + 1] == "0":
                    freq = freq1
                i += 1
                y = 0
                frames = []
                while y < 44100 * time:
                    frames.append(np.sin(y * 2 * freq * np.pi * 1/44100) * 22050)
                    y = y + 1
                player.write(frames)
            player.drain()  
    #If no input just end
    except Exception: 
        break  

